<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        @yield('title')
    </title>

    <link rel="stylesheet" href="{{ asset('modules/theme/admin/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ asset('modules/theme/admin/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('modules/theme/admin/css/custom_theme.css') }}">
    <link rel="stylesheet" href="{{ asset('modules/theme/admin/css/external_packages.css') }}">
    <script type="stylesheet" src="{{ asset('modules/theme/admin/toastr/toastr.min.css') }}"></script>
    <link rel="stylesheet" href="{{ asset('modules/theme/admin/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    @yield('extra-css')
</head>

<body class="hold-transition sidebar-mini text-sm layout-fixed sidebar-collapse">
    <div class="wrapper">
        @if (Auth::guard('admin')->check())
            @include('theme::layouts.admin.partials.nav')
            @include('theme::layouts.admin.partials.sidebar')
            <div class="content-wrapper">
                <div class="content-header">
                    @yield('content-header')
                </div>
                <div class="content">
                    @include('theme::layouts.admin.partials.notification')
                    @yield('content')
                </div>
            </div>
            @include('theme::layouts.admin.partials.footer')
        @endif
    </div>
    @yield('before-js')
    <script type="text/javascript" src="{{ asset('modules/theme/js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('modules/theme/js/jquery-ui.js') }}"></script>
    <script type="text/javascript" src="{{ asset('modules/theme/admin/bootstrap/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('modules/theme/admin/js/adminlte.min.js') }}"></script>
    <script type="text/javascript"
        src="{{ asset('modules/theme/admin/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('modules/theme/js/jquery.validate.js') }}"></script>
    <script type="text/javascript" src="{{ asset('modules/core/js/admin/custom.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script type="text/javascript" src="{{ asset('modules/theme/admin/js/custom-admin.js') }}"></script>
    <script type="text/javascript" src="{{ asset('modules/theme/admin/js/external_packages.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    @stack('after-js-stack')
    @yield('after-js');
    <script>
        Echo.private("Modules.Admin.Entities.Admin.{{ auth()->guard('admin')->user()->id }}")
            .notification((notification) => {
                Swal.fire({
                    position: 'top-end',
                    title: notification.message,
                    showConfirmButton: true,
                })
            });
    </script>
</body>

</html>
