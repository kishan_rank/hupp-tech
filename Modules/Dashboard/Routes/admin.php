<?php


use Illuminate\Support\Facades\Route;

Route::prefix('dashboard')->group(function() {
    Route::get('/', [
        'as' => 'admin.dashboard.index',
        'uses' => 'DashboardController@index',
        'middleware' => ['auth:admin']
    ]);
});