<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::prefix('employees')->group(function () {
    Route::get('/', 'EmployeeController@index')->name('employees.index')->middleware('auth:employee');
    Route::post('/apply-leave', 'EmployeeController@applyLeave')->name('employees.apply-leave')->middleware('auth:employee');
    Route::get('/profile', 'EmployeeController@profile')->name('employees.profile')->middleware('auth:employee');
    Route::post('/update-profile', 'EmployeeController@updateProfile')->name('employees.update_profile')->middleware('auth:employee');
    Route::post('/update-password', 'EmployeeController@updatePassword')->name('employees.update_password')->middleware('auth:employee');
});
