<?php

use Illuminate\Support\Facades\Route;

Route::prefix('employees')->group(function () {
    Route::get('/', [
        'as' => 'admin.employees.index',
        'uses' => 'EmployeeController@index',
        'middleware' => 'auth:admin'
    ]);

    Route::post('/get', [
        'as' => 'admin.employees.get',
        'uses' => 'EmployeeController@get',
        'middleware' => 'auth:admin'
    ]);

    Route::get('/approve/{id}', [
        'as' => 'admin.employees.approve',
        'uses' => 'EmployeeController@approve',
        'middleware' => 'auth:admin'
    ]);

    Route::get('/delete/{id}', [
        'as' => 'admin.employees.destroy',
        'uses' => 'EmployeeController@destroy',
        'middleware' => 'auth:admin'
    ]);
});

Route::prefix('leaves')->group(function () {
    Route::get('/', [
        'as' => 'admin.leaves.index',
        'uses' => 'LeaveController@index',
        'middleware' => 'auth:admin'
    ]);

    Route::post('/get', [
        'as' => 'admin.leaves.get',
        'uses' => 'LeaveController@get',
        'middleware' => 'auth:admin'
    ]);

    Route::get('/approve/{id}', [
        'as' => 'admin.leaves.approve',
        'uses' => 'LeaveController@approve',
        'middleware' => 'auth:admin'
    ]);

    Route::get('/reject/{id}', [
        'as' => 'admin.leaves.reject',
        'uses' => 'LeaveController@reject',
        'middleware' => 'auth:admin'
    ]);

});
