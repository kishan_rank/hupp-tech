<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Employee\Entities\EmployeeLeave;

class CreateEmployeeLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $employeeLeave = new EmployeeLeave();
        Schema::create($employeeLeave->getTable(), function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('employee_id');
            $table->string('subject');
            $table->date('start_date');
            $table->date('end_date');
            $table->text('reason');
            $table->tinyInteger('status');
            $table->timestamps();
            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $employeeLeave = new EmployeeLeave();
        Schema::dropIfExists($employeeLeave->getTable());
    }
}
