<?php

namespace Modules\Employee\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\Core\Entities\Traits\BaseModelTrait;
use Modules\Employee\Entities\Traits\Admin\Attribute\EmployeeAttribute;

class Employee extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use BaseModelTrait;
    use EmployeeAttribute;

    protected $table = 'employees';

    const IMAGE_STORAGE_PATH = 'modules/employee/image/';

    const REDIRECT_TO = '/';
    const VERIFIED_YES_TEXT = 'Yes';
    const VERIFIED_NO_TEXT = 'No';

    protected $fillable = [
        'name', 'email', 'is_verified', 'image', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getImageAttribute($logo)
    {
        $filePath = 'storage/' . self::IMAGE_STORAGE_PATH . $logo;
        if (file_exists(public_path($filePath))  && !is_dir($filePath)) {
            return asset($filePath);
        }
        return false;
    }

    public function leaves()
    {
        return $this->hasMany(EmployeeLeave::class, 'employee_id');
    }

    public function isLeaveAlreadyTaken($startDate, $endDate)
    {
        $instance = $this->leaves()
                        ->getQuery()
                        ->where([
                            ['start_date', '=', $startDate],
                            ['end_date', '=', $endDate],
                            ['status', '!=', EmployeeLeave::LEAVE_STATUS_REJECTED]]);
        return $instance->first();
    }
}
