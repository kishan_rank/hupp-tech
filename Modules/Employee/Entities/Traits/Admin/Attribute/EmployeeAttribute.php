<?php

namespace Modules\Employee\Entities\Traits\Admin\Attribute;

use Modules\Employee\Entities\Employee;

trait EmployeeAttribute
{
    public function getActionButtonsAttribute()
    {
        $data =  '<div class="btn-group action-btn">';
        $data .= $this->getDeleteButtonAttribute('admin.employees.destroy');
        if (!$this->is_verified) {
            $data .= $this->getApproveEmployeeAttribute('admin.employees.approve');
        }
        $data .= '</div>';
        return $data;
    }

    public function getIsVerifiedForGridAttribute()
    {
        if ($this->is_verified) {
            return '<label class="badge badge-success">' . Employee::VERIFIED_YES_TEXT . '</label>';
        }
        return '<label class="badge badge-danger">' . Employee::VERIFIED_NO_TEXT . '</label>';
    }

    public function getApproveEmployeeAttribute($route)
    {
        return '<a href="' . route($route, $this) . '" title="Approve Employee" class="btn btn-xs btn-flat btn-info mr-2">Approve</a>';
    }
}
