<?php

namespace Modules\Employee\Entities\Traits\Admin\Attribute;

use Modules\Employee\Entities\EmployeeLeave;

trait LeaveAttribute
{
    public function getActionButtonsAttribute()
    {
        $data =  '<div class="btn-group action-btn">';
        if (!$this->status) {
            $data .= $this->getApproveLeaveButton('admin.leaves.approve');
            $data .= $this->getRejectLeaveButton('admin.leaves.reject');
        } else {
            return '-';
        }
        $data .= '</div>';
        return $data;
    }

    public function getStatusForGridAttribute()
    {
        if (!$this->status) {
            return '<label class="badge badge-warning">' . EmployeeLeave::PENDING_STATUS_TEXT . '</label>';
        } elseif ($this->status == 1) {
            return '<label class="badge badge-success">' . EmployeeLeave::APPROVED_STATUS_TEXT . '</label>';
        } else {
            return '<label class="badge badge-danger">' . EmployeeLeave::REJECTED_STATUS_TEXT . '</label>';
        }
    }

    public function getApproveLeaveButton($route)
    {
        return '<a href="' . route($route, $this) . '" title="Approve Leave" class="btn btn-xs btn-flat btn-success mr-2">Approve</a>';
    }

    public function getRejectLeaveButton($route)
    {
        return '<a href="' . route($route, $this) . '" title="Reject Leave" class="btn btn-xs btn-danger btn-info mr-2">Reject</a>';
    }
}
