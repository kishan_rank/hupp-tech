<?php

namespace Modules\Employee\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Core\Entities\Traits\BaseModelTrait;
use Modules\Employee\Entities\Traits\Admin\Attribute\LeaveAttribute;

class EmployeeLeave extends Model
{
    use BaseModelTrait, LeaveAttribute;

    protected $table = 'employee_leaves';

    const LEAVE_STATUS_PENDING = 0;
    const LEAVE_STATUS_ACCEPTED = 1;
    const LEAVE_STATUS_REJECTED = 2;

    const PENDING_STATUS_TEXT = 'Pending';
    const APPROVED_STATUS_TEXT = 'Approved';
    const REJECTED_STATUS_TEXT = 'Rejected';

    protected $fillable = ['employee_id', 'subject', 'start_date', 'end_date', 'reason', 'status'];

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    public function getEmployeeLeaveDataForGrid()
    {
        return EmployeeLeave::query()
            ->leftJoin('employees', 'employee_leaves.employee_id', '=', 'employees.id')
            ->select(
                'employee_leaves.*',
                DB::raw('employees.name as name'),
            )
            ->get();
    }
}
