<?php

namespace Modules\Employee\Http\Controllers;

use App\Notifications\LeaveUpdateNotification;
use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Modules\Admin\Entities\Admin;
use Modules\Core\Http\Controllers\Admin\CoreController;
use Modules\Employee\Entities\Employee;
use Modules\Employee\Entities\EmployeeLeave;
use Modules\Employee\Http\Requests\ApplyLeaveRequest;
use Modules\Employee\Http\Requests\UpdateProfileRequest;

class EmployeeController extends CoreController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('employee::employee.index');
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function applyLeave(ApplyLeaveRequest $request)
    {
        try {
            $validatedData = $request->validated();

            if (strtotime($validatedData['start_date']) > strtotime($validatedData['end_date'])) {
                throw new Exception('Start date can not be greater then End date.');
            }

            $employee = $this->getCurrentEmployee();
            if (!$employee || !$employee->id) {
                throw new Exception('Invalid Employee Details');
            }

            $employee = Employee::findOrFail($employee->id);
            $isAlreadyLeaveTaken = $employee->isLeaveAlreadyTaken($validatedData['start_date'], $validatedData['end_date']);

            if ($isAlreadyLeaveTaken && $isAlreadyLeaveTaken->id) {
                throw new Exception('Leave has been already applied for the same time period');
            }

            $validatedData['employee_id'] = $employee->id;
            $validatedData['status'] = EmployeeLeave::LEAVE_STATUS_PENDING;

            $employeeLeave = EmployeeLeave::create($validatedData);

            $admin = Admin::first();
            if ($employeeLeave && $employeeLeave->id && $admin) {
                $admin->notify(new LeaveUpdateNotification("New Leave has been added by $employee->name, Please update leave status from <a href=" . route('admin.leaves.index') . ">Manage Leaves</a>."));
                return $this->successRedirect('employees.index', 'Leave request added successfully.');
            }
            return $this->errorRedirect('employees.index', trans('core::core.messages.something_wrong'));
        } catch (\Throwable $e) {
            return $this->backWithError($e->getMessage());
        }
    }

    public function getCurrentEmployee()
    {
        return auth()->guard('employee')->user();
    }

    public function profile()
    {
        try {
            $employee = auth()->guard('employee')->user();
            return view('employee::employee.profile', compact('employee'));
        } catch (\Throwable $e) {
            return $this->errorRedirect('employee.index', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function updateProfile(UpdateProfileRequest $request)
    {
        try {
            $data = $request->all();
            if ($request->file('image')) {
                $imageData['file'] = request()->file('image');
                $imageData['storage_path'] = Employee::IMAGE_STORAGE_PATH;
                $imageData['disk'] = 'public';
                $filename = $this->saveImage($imageData);
                $data['image'] = $filename;
            }
            $employee = Employee::findOrFail(auth()->guard('employee')->user()->id);

            $employee->update($data);
            return $this->successRedirect('employees.profile', 'Data saved successfully.');
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    public function updatePassword(Request $request)
    {
        $rules = [
            'password' => 'required|string|confirmed|min:6|max:255',
            'current_password' => 'required|min:6'
        ];
        $request->validate($rules);

        $currentEmployee = auth()->guard('employee')->user();
        $employee = Employee::findOrFail($currentEmployee->id);

        if (Hash::check($request->current_password, $currentEmployee->password)) {
            $employee->password = bcrypt($request->password);
            if ($employee->save()) {
                Auth::guard('employee')->logout();
                return $this->successRedirect('employees.login', 'Password updated successfully, please login with new password', 200);
            }
        }
        return $this->backWithError("Invalid current password");
    }
}
