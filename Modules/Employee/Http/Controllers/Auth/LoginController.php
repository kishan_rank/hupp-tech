<?php

namespace Modules\Employee\Http\Controllers\Auth;

use Illuminate\Routing\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use ThrottlesLogins;

    public $maxAttempts = 5;

    public $decayMinutes = 3;

    public function __construct()
    {
        $this->middleware('guest:employee')->except('logout');
    }

    public function showLoginForm()
    {
        return view('employee::auth.login');
    }

    public function login(Request $request)
    {
        $this->validator($request);

        //check if the user has too many login attempts.
        if ($this->hasTooManyLoginAttempts($request)) {
            //Fire the lockout event
            $this->fireLockoutEvent($request);

            //redirect the user back after lockout.
            return $this->sendLockoutResponse($request);
        }

        if (Auth::guard('employee')->attempt($request->only('email', 'password'), $request->filled('remember'))) {
            return $this->authenticated($request, Auth::guard('employee')->user());
        }

        $this->incrementLoginAttempts($request);

        return $this->loginFailed();
    }

    protected function authenticated(Request $request, $user)
    {
        if ($user && !$user->is_verified) {
            Auth::guard('employee')->logout();
            $message = 'Your account is not Approved by admin, please contact support team!';
            return redirect(route('employees.login'))->with(['error' => $message]);
        }
        return redirect()->route('employees.index');
    }

    public function logout()
    {
        Auth::guard('employee')->logout();
        return redirect()
            ->route('employees.login')
            ->with('success', 'Employee has been logged out!');
    }

    private function validator(Request $request)
    {
        //validation rules.
        $rules = [
            'email'    => 'required|email|exists:employees|min:5|max:191',
            'password' => 'required|string|min:4|max:255',

        ];

        //custom validation error messages.
        $messages = [
            'email.exists' => 'These credentials do not match our records.',
        ];

        //validate the request.
        $request->validate($rules, $messages);
    }

    private function loginFailed()
    {
        $notification = array(
            'error' => 'Password Mismatch, please check credentials and try again!',
        );
        return redirect()
            ->back()
            ->withInput()
            ->with($notification);
    }

    public function username()
    {
        return 'email';
    }
}
