<?php

namespace Modules\Employee\Http\Controllers\Auth;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Modules\Core\Http\Controllers\Admin\CoreController;
use Modules\Employee\Entities\Employee;

class RegisterController extends CoreController
{
    use RegistersUsers;

    public function __construct()
    {
        $this->middleware('guest:employee')->except('logout');
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function showRegisterForm()
    {
        return view('employee::auth.register');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function register(Request $request)
    {
        $this->validator($request);
        $user = $this->create($request->all());
        $this->guard('employee')->logout();
        $notification = array(
            'success' => 'Thank you for Registering with us. please wail till admin approves your account.',
        );
        return redirect(route('employees.login'))->with($notification);
    }

    protected function validator(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email'    => 'required|email|min:5|max:191|unique:employees,email',
            'password' => 'required|string|confirmed|min:4|max:255',
            'image' => 'required|image'
        ];
        $request->validate($rules);
    }

    protected function create(array $data)
    {
        $imageData['file'] = request()->file('image');
        $imageData['storage_path'] = Employee::IMAGE_STORAGE_PATH;
        $imageData['disk'] = 'public';

        $filename = $this->saveImage($imageData);
        $data['image'] = $filename;
        $user = Employee::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'is_verifies' => false,
            'image' => $data['image'],
            'password' => Hash::make($data['password'])
        ]);

        return $user;
    }
}
