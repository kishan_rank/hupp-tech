<?php

namespace Modules\Employee\Http\Controllers\Admin;

use App\Events\LeaveUpdateMessage;
use App\Notifications\LeaveUpdateNotification;
use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Modules\Core\Http\Controllers\Admin\CoreController;
use Modules\Employee\Emails\LeaveStatusUpdatedMail;
use Modules\Employee\Entities\EmployeeLeave;
use Yajra\DataTables\Facades\DataTables;

class LeaveController extends CoreController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('employee::admin.leave.index');
    }

    /**
     *  return dynamic response for jquery detatables
     */
    public function get(Request $request)
    {
        try {
            if ($request->ajax()) {
                $leave = new EmployeeLeave();
                $data = $leave->getEmployeeLeaveDataForGrid();
                return DataTables::of($data)
                    ->addIndexColumn()
                    ->editColumn('created_at', function ($leave) {
                        return date('d-m-Y H:i:s', strtotime($leave->created_at));
                    })
                    ->editColumn('status', function ($leave) {
                        return $leave->status_for_grid;
                    })
                    ->addColumn('action', function ($leave) {
                        return $leave->action_buttons;
                    })
                    ->rawColumns(['action', 'status'])
                    ->make(true);
            }
            return $this->errorMessageResponse(['message' => trans('core::core.messages.something_wrong')]);
        } catch (\Throwable $e) {
            return $this->errorMessageResponse(['message' => $e->getMessage()], $e->getCode());
        }
    }
    // Approve leave
    public function approve($id)
    {
        try {
            $leave = EmployeeLeave::findOrFail($id);
            if (!isset($leave->employee)) {
                throw new Exception('Invalid Employee Found.');
            }
            $leave->status = EmployeeLeave::LEAVE_STATUS_ACCEPTED;
            $leave->save();

            $employee = $leave->employee;

            if ($leave && $leave->status == EmployeeLeave::LEAVE_STATUS_ACCEPTED && $employee) {
                $employee->notify(new LeaveUpdateNotification('Your leave has been approved. <br> Leave Subject : ' . $leave->subject));
                Mail::to($leave->employee->email)->send(new LeaveStatusUpdatedMail($leave));
                return $this->successRedirect('admin.leaves.index', trans('employee::employee.messages.leave_approved'));
            }
            return $this->errorRedirect('admin.leaves.index', trans('core::core.messages.something_wrong'));
        } catch (\Throwable $e) {
            return $this->errorRedirect('admin.leaves.index', $e->getMessage());
        }
    }

    // Reject leave
    public function reject($id)
    {
        try {
            $leave = EmployeeLeave::findOrFail($id);
            if (!isset($leave->employee)) {
                throw new Exception('Invalid Employee Found.');
            }
            $leave->status = EmployeeLeave::LEAVE_STATUS_REJECTED;
            $leave->save();

            $employee = $leave->employee;

            if ($leave && $leave->status == EmployeeLeave::LEAVE_STATUS_REJECTED && $employee) {
                $employee->notify(new LeaveUpdateNotification('Your leave has been rejected. <br> Leave Subject : ' . $leave->subject));
                Mail::to($leave->employee->email)->send(new LeaveStatusUpdatedMail($leave));
                return $this->successRedirect('admin.leaves.index', trans('employee::employee.messages.leave_rejected'));
            }
            return $this->errorRedirect('admin.leaves.index', trans('core::core.messages.something_wrong'));
        } catch (\Throwable $e) {
            return $this->errorRedirect('admin.leaves.index', $e->getMessage());
        }
    }
}
