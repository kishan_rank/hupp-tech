<?php

namespace Modules\Employee\Http\Controllers\Admin;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Admin\CoreController;
use Modules\Employee\Entities\Employee;
use Yajra\DataTables\Facades\DataTables;

class EmployeeController extends CoreController
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        try {
            return view('employee::admin.index');
        } catch (\Throwable $e) {
            return $this->errorRedirect('admin.dashboard.index', $e->getMessage());
        }
    }

    /**
     *  return dynamic response for jquery detatables
     */
    public function get(Request $request)
    {
        try {
            if ($request->ajax()) {
                $data = Employee::latest();
                return DataTables::of($data)
                    ->addIndexColumn()
                    ->editColumn('created_at', function ($admin) {
                        return date('d-m-Y H:i:s', strtotime($admin->created_at));
                    })
                    ->editColumn('is_verified', function ($admin) {
                        return $admin->is_verified_for_grid;
                    })
                    ->addColumn('action', function ($admin) {
                        return $admin->action_buttons;
                    })
                    ->rawColumns(['action', 'is_verified'])
                    ->make(true);
            }
            return $this->errorMessageResponse(['message' => trans('core::core.messages.something_wrong')]);
        } catch (\Throwable $e) {
            return $this->errorMessageResponse(['message' => $e->getMessage()], $e->getCode());
        }
    }

    /**
     * Approve employee account
     * @param int $id
     * @return Renderable
     */
    public function approve($id)
    {
        try {
            $employee = Employee::findOrFail($id);
            $employee->is_verified = 1;
            $employee->save();

            if ($employee->is_verified == 1) {
                return $this->successRedirect('admin.employees.index', trans('employee::employee.messages.approve_success'));
            }
            return $this->errorRedirect('admin.employees.index', trans('core::core.messages.something_wrong'));
        } catch (\Throwable $e) {
            return $this->errorRedirect('admin.employees.index', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        try {
            $employee = Employee::findOrFail($id);
            $employee->delete();
            return $this->successRedirect('admin.employees.index', 'Employee data deleted successfully.');
        } catch (\Throwable $e) {
            return $this->errorRedirect('admin.employees.index', $e->getMessage());
        }
    }
}
