<?php

return [
    'titles' => [
        'manage_employee' => 'Manage Employee',
        'manage_employee_leaves' => 'Manage Leaves'
    ],
    'grid' => [
        'header' => [
            'no' => 'No.',
            'name' => 'Employee Name',
            'email' => 'Email',
            'is_verified' => 'Is Verified',
            'subject' => 'Subject',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'status' => 'Status',
            'applied_date' => 'Applied Date',
            'created_at' => 'Created At',
            'action' => 'Action'
        ],
    ],
    'messages' => [
        'approve_success' => 'Employee Status Approved Successfully.',
        'leave_approved' => 'Leave Approved successfully.',
        'leave_rejected' => 'Leaved Has been rejected.'
    ],
];
