@extends('theme::layouts.employee.master')

@section('title')
    {{ 'Employee Login' }}
@stop

@section('content')
    <div class="section">
        <div class="container col-md-12">
            <div class="auth-wrapper">
                <div class="auth-description bg-cover bg-center dark-overlay dark-overlay-2"
                    style="background-image: url('https://via.placeholder.com/1280x1560')">
                </div>
                <div class="auth-form">
                    <h2>Sign In</h2>
                    <form method="POST" action="{{ route('employees.postLogin') }}">
                        @csrf
                        <div class="form-group">
                            <input type="email" class="form-control form-control-light @error('email') is-invalid @enderror"
                                placeholder="Email Address" name="email" value="{{ old('email') }}" required>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="password"
                                class="form-control form-control-light @error('password') is-invalid @enderror"
                                placeholder="Password" name="password" value="" required>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Sign In</button>
                        <p>New User? <a href="{{ route('employees.register') }}">Register</a> </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    @include('employee::auth.includes.sweetalert')
@endsection
