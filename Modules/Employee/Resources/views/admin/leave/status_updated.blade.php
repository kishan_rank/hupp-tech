<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Leave Update</title>
</head>

<body>
    <div>
        <h3>Hello {{ $leave->employee->name }},</h3>
        <h3>Leave status updated</h3>
        <p>Your Leave has been {{ $leave->status == 1 ? 'Approved' : 'Rejected' }}</p>

        @if ($leave->status == 1)
            <p>Start Date : {{ $leave->start_date }}</p>
            <p>End Date : {{ $leave->end_date }}</p>
        @elseif ($leave->status == 2)
            <p>Please contact admin for the reason of leave rejection.</p>
        @endif
    </div>
</body>

</html>
