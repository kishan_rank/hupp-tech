@extends('theme::layouts.admin.master')

@section('title')
    {{ trans('employee::employee.titles.manage_employee_leaves') }}
@endsection

@section('extra-css')
    @include('theme::asset.admin.css.datatable')
@stop

@section('content-header')
    <!-- Content Header (Page header) -->
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{ trans('employee::employee.titles.manage_employee_leaves') }}</h1>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- Main content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info card-outline">
                    <div class="card-body">
                        <div class="table-responsive data-table-wrapper">
                            <table id="employees-leaves-table" class="table table-condensed table-hover table-bordered"
                                width="100%">
                                <thead class="transparent-bg">
                                    <tr>
                                        <th>{{ trans('employee::employee.grid.header.no') }}</th>
                                        <th>{{ trans('employee::employee.grid.header.name') }}</th>
                                        <th>{{ trans('employee::employee.grid.header.subject') }}</th>
                                        <th>{{ trans('employee::employee.grid.header.start_date') }}</th>
                                        <th>{{ trans('employee::employee.grid.header.end_date') }}</th>
                                        <th>{{ trans('employee::employee.grid.header.status') }}</th>
                                        <th>{{ trans('employee::employee.grid.header.applied_date') }}</th>
                                        <th>{{ trans('employee::employee.grid.header.action') }}</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    @include('theme::layouts.admin.modals.confirm')
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@stop

@section('after-js')
    @include('theme::asset.admin.js.datatable')
    <script>
        $(function() {
            var dataTable = $('#employees-leaves-table').DataTable({
                serverSide: true,
                ajax: {
                    url: '{{ route('admin.leaves.get') }}',
                    type: 'post'
                },
                lengthMenu: [20, 30, 50, 100, 200],
                pageLength: 20,
                columns: [{
                        data: 'DT_RowIndex'
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'subject'
                    },
                    {
                        data: 'start_date'
                    },
                    {
                        data: 'end_date'
                    },
                    {
                        data: 'status'
                    },
                    {
                        data: 'created_at'
                    },
                    {
                        data: 'action',
                        searchable: false,
                        sortable: false
                    },
                ],
                searchDelay: 500,
            });
        });
    </script>
@stop
