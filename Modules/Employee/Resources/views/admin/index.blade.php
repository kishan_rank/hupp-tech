@extends('theme::layouts.admin.master')

@section('title')
    {{ trans('employee::employee.titles.manage_employee') }}
@endsection

@section('extra-css')
    @include('theme::asset.admin.css.datatable')
@stop

@section('content-header')
    <!-- Content Header (Page header) -->
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{ trans('employee::employee.titles.manage_employee') }}</h1>
            </div>
        </div>
    </div>
@stop

@section('content')
    <!-- Main content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info card-outline">
                    <div class="card-body">
                        <div class="table-responsive data-table-wrapper">
                            <table id="employees-table" class="table table-condensed table-hover table-bordered"
                                width="100%">
                                <thead class="transparent-bg">
                                    <tr>
                                        <th>{{ trans('employee::employee.grid.header.no') }}</th>
                                        <th>{{ trans('employee::employee.grid.header.name') }}</th>
                                        <th>{{ trans('employee::employee.grid.header.email') }}</th>
                                        <th>{{ trans('employee::employee.grid.header.is_verified') }}</th>
                                        <th>{{ trans('employee::employee.grid.header.created_at') }}</th>
                                        <th>{{ trans('employee::employee.grid.header.action') }}</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    @include('theme::layouts.admin.modals.confirm')
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@stop

@section('after-js')
    @include('theme::asset.admin.js.datatable')
    <script>
        $(function() {
            var dataTable = $('#employees-table').DataTable({
                serverSide: true,
                ajax: {
                    url: '{{ route('admin.employees.get') }}',
                    type: 'post'
                },
                lengthMenu: [20, 30, 50, 100, 200],
                pageLength: 20,
                columns: [{
                        data: 'id'
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'email'
                    },
                    {
                        data: 'is_verified'
                    },
                    {
                        data: 'created_at'
                    },
                    {
                        data: 'action',
                        searchable: false,
                        sortable: false
                    },
                ],
                searchDelay: 500,
            });
        });
    </script>
@stop
