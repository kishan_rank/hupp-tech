@extends('theme::layouts.employee.master')

@section('title')
    {{ 'Apply for Leave' }}
@stop

@section('content')
    <div class="section">
        <div class="imgs-wrapper">
        </div>
        <div class="container col-md-12">
            <div class="auth-wrapper">
                <div class="auth-description bg-cover bg-center dark-overlay dark-overlay-2"
                    style="background-image: url('https://via.placeholder.com/1280x1560')">
                </div>
                <div class="auth-form">
                    <h2>Apply for Leave</h2>
                    <form method="POST" action="{{ route('employees.apply-leave') }}" enctype='multipart/form-data'>
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control form-control-light @error('subject') is-invalid @enderror"
                                placeholder="Subject" name="subject" value="{{ old('subject') }}" required>
                            @error('subject')
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="text"
                                class="form-control form-control-light @error('start_date') is-invalid @enderror"
                                placeholder="Start Date" id="start_date" name="start_date" value="{{ old('start_date') }}"
                                required>
                            @error('start_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="text"
                                class="form-control form-control-light @error('end_date') is-invalid @enderror"
                                placeholder="End Date" id="end_date" name="end_date" value="{{ old('end_date') }}"
                                required>
                            @error('end_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <textarea class="form-control form-control-light @error('reason') is-invalid @enderror"
                                placeholder="Reason" name="reason" value="{{ old('reason') }}" required></textarea>
                            @error('reason')
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('before-js')
@endpush
@push('script')
    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery("#start_date").datepicker({
                minDate: 0,
                dateFormat: 'yy-mm-dd',
                onSelect: function(selected) {
                    var dt = new Date(selected);
                    dt.setDate(dt.getDate());
                    $("#end_date").datepicker("option", "minDate", dt);
                }
            });
            jQuery("#end_date").datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: 0,
            });
        });
    </script>
    @include('employee::auth.includes.sweetalert')
    <script>
        Echo.private("Modules.Employee.Entities.Employee.{{ auth()->guard('employee')->user()->id }}")
            .notification((notification) => {
                Swal.fire({
                    position: 'top-end',
                    title: notification.message,
                    showConfirmButton: true,
                })
            });
    </script>
@endpush
