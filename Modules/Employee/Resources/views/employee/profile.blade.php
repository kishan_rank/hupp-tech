@extends('theme::layouts.employee.master')

@section('title')
    {{ 'Update Profile' }}
@stop

@section('content')
    <div class="section">
        <div class="imgs-wrapper">
        </div>
        <div class="container col-md-12">
            <div class="auth-wrapper">
                <div class="auth-description bg-cover bg-center dark-overlay dark-overlay-2"
                    style="background-image: url('https://via.placeholder.com/1280x1560')">
                </div>
                <div class="auth-form">
                    <h2>Update Profile</h2>
                    <form method="POST" action="{{ route('employees.update_profile') }}" enctype='multipart/form-data'>
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control form-control-light @error('name') is-invalid @enderror"
                                placeholder="Name" name="name" value="{{ $employee->name }}" required>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control form-control-light @error('email') is-invalid @enderror"
                                placeholder="Email Address" name="email" value="{{ $employee->email }}" required>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="file" class="form-control form-control-light @error('image') is-invalid @enderror"
                                placeholder="Image" name="image" value="">
                            @error('image')
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>

                    <form method="POST" action="{{ route('employees.update_password') }}" enctype='multipart/form-data'>
                        @csrf
                        <div>
                            <h2>Update Password</h2>
                        </div>
                        <div class="form-group">
                            <input type="password"
                                class="form-control form-control-light @error('password') is-invalid @enderror"
                                placeholder="Password" name="password" value="" required>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="password"
                                class="form-control form-control-light @error('password_confirmation') is-invalid @enderror"
                                placeholder="Confirm Password" name="password_confirmation" value="">
                        </div>
                        <div class="form-group">
                            <input type="password"
                                class="form-control form-control-light @error('current_password') is-invalid @enderror"
                                placeholder="Current Password" name="current_password" value="" required>
                            @error('current_password')
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    @include('employee::auth.includes.sweetalert')
@endsection
