<?php

namespace Modules\Employee\Sidebar;

use Modules\Core\Foundations\Menu;

class MenuSidebar
{
    protected $_menu;

    public function __construct()
    {
        $this->_menu = app(Menu::class);
        $this->initMenu();
    }

    public function initMenu()
    {
        $menuItems = [
            "group" => "core::core.menu.single.employee",
            "title" => "employee::employee.titles.manage_employee",
            "route" => "admin.employees.index",
            "icon" => "fas fa-users nav-icon",
            "active_actions" => [
                "admin.employees.index"
            ],
            "order" => 5,
        ];

        $menuItems2 = [
            "group" => "core::core.menu.single.employee_leaves",
            "title" => "employee::employee.titles.manage_employee_leaves",
            "route" => "admin.leaves.index",
            "icon" => "fas fa-circle nav-icon",
            "active_actions" => [
                "admin.leaves.index"
            ],
            "order" => 6,
        ];

        $this->_menu->addMenuItem($menuItems);
        $this->_menu->addMenuItem($menuItems2);
    }
}
