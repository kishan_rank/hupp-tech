<?php

namespace Modules\Employee\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LeaveStatusUpdatedMail extends Mailable
{
    use Queueable, SerializesModels;
    public $leave;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($leave)
    {
        $this->leave = $leave;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $leaveStatus = ($this->leave->status == 1) ? 'Approved' : 'Rejected';
        $subject = 'Your leave has been ' . $leaveStatus;
        $mail = $this->from(config('mail.from.address'), config('mail.from.name'))
            ->subject($subject)
            ->view('employee::admin.leave.status_updated')
            ->with([
                'leave' => $this->leave
            ]);

        $mail->delay(now()->addSeconds(3));
        return $mail;
    }
}
