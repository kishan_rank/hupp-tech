<?php

if (!function_exists('getStatusOptions')) {

    function getStatusOptions()
    {
        return [
            '0' => 'Disable',
            '1' => 'Enable'
        ];
    }
}

if (!function_exists('getPerPageRecordsOptions')) {

    function getPerPageRecordsOptions()
    {
        return [20, 30, 50, 100, 200];
    }
}

if (!function_exists('getBackButton')) {

    function getBackButton($route)
    {
        $route = route($route);
        return '<a class="btn btn-secondary" href="' . $route . '">Back</a>';
    }
}

if (!function_exists('getCancelButton')) {

    function getCancelButton($route)
    {
        $route = route($route);
        return '<a class="btn btn-secondary" href="' . $route . '">Cancel</a>';
    }
}

if (!function_exists('getDefaultProductImage')) {

    function getDefaultProductImage()
    {
        return asset('storage/default/product/dummy-image.png');
    }
}


if (!function_exists('getAdminRoutePrefix')) {

    function getAdminRoutePrefix()
    {
        $prefix = config('core.admin_route_prefix', 'admin');
        return $prefix;
    }
}