<?php

namespace Modules\Core\Repositories\Eloquent;

use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Modules\Core\Repositories\BaseRepository;
use Modules\Core\Entities\Activities;

/**
 * Class EloquentCoreRepository
 *
 * @package Modules\Core\Repositories\Eloquent
 */
abstract class EloquentBaseRepository implements BaseRepository
{
    public $_uploadParams = array();
    public function setUploadParams($params)
    {
        $this->_uploadParams = $params;
        return $this;
    }

    public function uploadImage($request)
    {
        $fileData = $this->uploadFileParameters($request);
        $storagePath = $fileData['storagePath'];
        $file = $fileData['file'];
        $thumbmail = $fileData['thumbmail'];
        $file->store($storagePath, ['disk' =>  'public']);
        $fileName = $file->hashName();
        $filePath = $storagePath . '/' . $file->hashName();
        $thumbPath = $storagePath . '/thumbnails/' . $file->hashName();
        if (isset($this->_uploadParams['thumbnail']) && $this->_uploadParams['thumbnail']) {
            $thumbmail->store($storagePath . '/thumbnails', ['disk' =>  'public']);
        }
        $request = $request->all();
        if (file_exists(public_path('storage') . '/' . $filePath)) {
            if (isset($this->_uploadParams['column']) && $this->_uploadParams['column']) {
                $request[$this->_uploadParams['column']] = $fileName;
            } else {
                $request[$this->_uploadParams['dbfield']] = $fileName;
                return $request;
            }
        } else {
            // return $request;
            throw new Exception('File not found or not uploaded properly please check storage permission.');
        }
    }

    public function uploadFileParameters($request)
    {
        $this->_uploadParams['module_name'] = strtolower($this->_uploadParams['module_name']);
        $path = 'app/public/' . $this->_uploadParams['module_name'] . '/';
        if (isset($this->_uploadParams['storage_path']) && $this->_uploadParams['storage_path']) {
            $path = 'app/public/' . $this->_uploadParams['storage_path'] . '/';
        }

        $thumbnailPath = $path . 'thumbnails/';
        if ((!isset($this->_uploadParams['dbfield']) && $this->_uploadParams['dbfield'] == null) && (isset($this->_uploadParams['column']) && $this->_uploadParams['column'])) {
            $this->_uploadParams['dbfield'] = $this->_uploadParams['column'];
        }

        if (isset($this->_uploadParams['column']) && $this->_uploadParams['column']) {
            $thumbmail = $file = $request->file($this->_uploadParams['column']);
        } else {
            $thumbmail = $file = $request->file($this->_uploadParams['dbfield']);
        }

        $storagePath = (isset($this->_uploadParams['storage_path']) && $this->_uploadParams['storage_path']) ? $this->_uploadParams['module_name'] . $this->_uploadParams['storage_path'] : $this->_uploadParams['module_name'];

        return ["storagePath" => $storagePath, "file" => $file, "thumbmail" => $thumbmail];
    }
}
