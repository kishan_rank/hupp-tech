<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Modules\Admin\Entities\Traits\Admin\Attribute\AdminAttribute;
use Modules\Admin\Notifications\Admin\ResetPassword;
use Modules\Core\Entities\Traits\BaseModelTrait;

class Admin extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;
    use BaseModelTrait;
    use AdminAttribute;

    const REDIRECT_TO = 'admin/dashboard';
    const ENABLED_TEXT = 'Enable';
    const DISABLED_TEXT = 'Disable';
    const CONFIRMED_YES_TEXT = 'Yes';
    const CONFIRMED_NO_TEXT = 'No';

    protected $table = 'admins';

    protected $fillable = [
        'name', 'email', 'password', 'active'
    ];

    protected $dates = ['deleted_at'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdminActivated()
    {
        return $this->confirmed;
    }

    public function adminActivationCode()
    {
        return $this->hasOne(AdminActivationCode::class);
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }
}
