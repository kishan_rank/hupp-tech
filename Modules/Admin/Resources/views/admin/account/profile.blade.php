@extends('theme::layouts.admin.master')

@section('title')
{{ trans('admin::admin.titles.admin_profile') }}
@endsection

@section('extra-css')
@stop

@section('content-header')
    <!-- Content Header (Page header) -->
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{ trans('admin::admin.titles.admin_profile') }}</h1>
            </div>
            <div class="col-sm-6"></div>
        </div>
    </div>
@stop

@section('content')
    <!-- Main content -->
    <div class="container-fluid">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-info">
                        <strong>Update Profile</strong>
                        <button type="button" name="update_admin_password" data-dismiss="modal" id="update_admin_password"
                            class="btn btn-primary btn-xs  mr-2 float-right">Change Password ?</button>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('admin.adminuser.profile.update') }}" method="post"
                            enctype="multipart/form-data">
                            {{ csrf_field() }}
                            @method('PUT')

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"
                                    value="{{ $admin->name }}" required>
                                @error('name')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="name">Email</label>
                                <input type="text" name="email" class="form-control @error('email') is-invalid @enderror"
                                    value="{{ $admin->email }}" required>
                                @error('email')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <div class="text-center">
                                    <button class="btn btn-success" type="submit">
                                        Update Profile
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @include('admin::admin.account.change-password')
        </div>
    </div>

    <!-- /.container-fluid -->
@stop

@section('after-js')
    <script>
        $(document).ready(function() {
            $('#update_admin_password').click(function() {
                $('.modal-title').text('Change Password');
                $('#updateAdminPasswordModal').modal('show');
            });

            $('#update_admin_password_form').on('submit', function(event) {
                event.preventDefault();
                $.ajax({
                    url: "{{ route('admin.adminuser.updatepassword') }}",
                    method: 'POST',
                    data: $(this).serialize(),
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.type == 'success') {
                            Toast.fire({
                                icon: 'success',
                                title: data.content.message
                            })
                            $('#update_admin_password_form')[0].reset();
                            $('#updateAdminPasswordModal').modal('hide');
                            window.setTimeout(function() {
                                window.location.href = "{{ route('admin.logout') }}";
                            }, 1500);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        var msg = "";
                        var data = JSON.parse(jqXHR.responseText);
                        var error = data.content.errors;
                        if (Array.isArray(error) && error.length > 0) {
                            for (i = 0; i < error.length; i++) {
                                msg += error[i] + "<br>";
                            }
                        }
                        Toast.fire({
                            icon: 'error',
                            title: msg,
                        })
                        $('#updateAdminPasswordModal').modal('hide');
                    }
                });
            });
        });
    </script>
@stop
