<?php

namespace Modules\Admin\Listeners\Admin;

use Illuminate\Support\Facades\Notification;
use Modules\Admin\Notifications\Admin\SendActivationCode;

class ActivationCodeListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $url = route('admin.activate.account', ['code' => $event->admin->adminActivationCode->code]);
        Notification::send($event->admin, new SendActivationCode($url));
    }
}
