Step to install
 -- composer install
 -- php artisan key:generate
create .env file and set db details

commands:
php artisan migrate
php artisan module:seed --class=AdminDatabaseSeeder Admin
php artisan storage:link

Admin credentials : huppadmin@gmail.com/huppadmin

For real time notifiaction update
php artisan websockets:serve