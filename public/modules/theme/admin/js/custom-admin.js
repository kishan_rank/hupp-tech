//! moment.js
// Add remove loading class on body element based on Ajax request status
const Toast = Swal.mixin({
	toast: true,
	position: 'top-end',
	timer: 3000
});

jQuery(document).on({
	ajaxStart: function () {
	},
	ajaxStop: function () {
		setTimeout(function () {
			$('.wrapper').waitMe('hide');
		}, 200);
	},
});

//send csrf token headers to all ajax request
jQuery.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});

jQuery.ajaxSetup({
	beforeSend: function () {
		$('.wrapper').waitMe({
			effect: 'stretch',
			text: 'Please Wait...',
		});
	},
});

///menu tree view
$(document).ready(function () {
	jQuery("ul.nav-treeview li.nav-item a.nav-link").each(function (index, element) {
		if (jQuery(element).hasClass("active")) {
			var parent = jQuery(element).parents("li.has-treeview");
			// parent.addClass("menu-open");
			parent.find("a.nav-group-link").addClass("active");
		}
	});
});

//datatables
//delete with swal

$(document).on('click', 'a.delete-entry', function (e) {
	Swal.fire(
		'Opps!',
		'You clicked the button!',
		'error'
	)
	e.preventDefault();
	var url = $(this).attr('href');

	const swalWithBootstrapButtons = Swal.mixin({
		customClass: {
			confirmButton: 'btn btn-success',
			cancelButton: 'btn btn-danger mr-3'
		},
		buttonsStyling: false
	});

	swalWithBootstrapButtons.fire({
		title: 'Are you sure?',
		text: "You won't be able to revert this!",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Yes, delete it!',
		cancelButtonText: 'No, cancel!',
		reverseButtons: true
	}).then((result) => {
		if (result.isConfirmed) {
			window.location.href = url;
		}
	});
});

$(document).on('click', 'a.disable-delete-message', function (e) {
	var message = $(this).data('message');
	Swal.fire(
		'Opps!',
		message,
		'error'
	)
	e.preventDefault();
});

$(document).on('click', 'a.disable-edit-message', function (e) {
	var message = $(this).data('message');
	Swal.fire(
		'Opps!',
		message,
		'error'
	)
	e.preventDefault();
});

function deleteWithConfirmation(any) {
	const swalWithBootstrapButtons = Swal.mixin({
		customClass: {
			confirmButton: 'btn btn-success',
			cancelButton: 'btn btn-danger'
		},
		buttonsStyling: false
	});

	swalWithBootstrapButtons.fire({
		title: 'Are you sure?',
		text: "You won't be able to revert this!",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Yes, delete it!',
		cancelButtonText: 'No, cancel!',
		reverseButtons: true
	}).then((result) => {
		if (result.isConfirmed) {
			swalWithBootstrapButtons.fire(
				'Deleted!',
				'Your file has been deleted.',
				'success'
			)
		}
	});
}